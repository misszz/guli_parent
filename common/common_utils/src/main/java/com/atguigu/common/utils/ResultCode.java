package com.atguigu.common.utils;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/1 23:10
 **/

public class ResultCode {
    public static Integer SUCCESS = 20000;

    public static Integer ERROR = 30000;
}
