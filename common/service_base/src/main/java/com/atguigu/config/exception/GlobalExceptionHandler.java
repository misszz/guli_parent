package com.atguigu.config.exception;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 统一异常处理类
 * @since 2020/9/4 22:27
 **/

import com.atguigu.common.utils.ExceptionUtil;
import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R error(Exception e){
        log.error(ExceptionUtil.getMessage(e));
        return R.error();
    }
}