package com.atguigu.ucenter.controller;

import com.atguigu.common.utils.JwtUtils;
import com.atguigu.config.exception.GuliException;
import com.atguigu.ucenter.entity.Member;
import com.atguigu.ucenter.service.MemberService;
import com.atguigu.ucenter.utils.ConstantPropertiesUtil;
import com.atguigu.ucenter.utils.HttpClientUtils;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/26 17:42
 **/
@Controller
@RequestMapping("/api/ucenter/wx")
@CrossOrigin
@Slf4j
public class WxApiController {

    @Autowired
    private MemberService memberService;

    /**
     * 2 获取扫描人信息 添加数据
     *
     * @param code
     * @param state
     * @return
     */
    @GetMapping("callback")
    public String callback(String code, String state) {
        //得到临时票据code
        System.out.println("code = " + code);
        System.out.println("state = " + state);
        //通过code 访问微信固定请求
        String baseAccessTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token" +
                "?appid=%s" +
                "&secret=%s" +
                "&code=%s" +
                "&grant_type=authorization_code";

        //拼接三个参数
        String accessTokenUrl = String.format(
                baseAccessTokenUrl,
                ConstantPropertiesUtil.WX_OPEN_APP_ID,
                ConstantPropertiesUtil.WX_OPEN_APP_SECRET,
                code
        );
        //通过httpclient请求拼接好的地址 获取accessToken和openid
        String accessTokenInfo = null;
        try {
            accessTokenInfo = HttpClientUtils.get(accessTokenUrl);
        } catch (Exception e) {
            throw new GuliException(20001, "获取accesstoken失败");
        }
        //把accessTokenInfo中字符串通过json工具包转换成map集合，通过key获取对应值
        Gson gson = new Gson();
        HashMap tokenInfoMap = gson.fromJson(accessTokenInfo, HashMap.class);
        String accessToken = (String) tokenInfoMap.get("access_token");
        String openId = (String) tokenInfoMap.get("openid");

        //根据openid从数据库中查询 判断当前用户是否曾经使用过微信登录
        Member member = memberService.getByOpenid(openId);
        if (member == null) {
            log.info("新用户注册");
            String userInfoUrl = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s";
            userInfoUrl = String.format(userInfoUrl, accessToken, openId);
            log.info("userInfoUrl:{}", userInfoUrl);
            String userInfo = null;
            try {
                userInfo = HttpClientUtils.get(userInfoUrl);
            } catch (Exception e) {
                throw new GuliException(20001, "获取用户信息失败");
            }
            HashMap<String, Object> map = gson.fromJson(userInfo, HashMap.class);
            //获取用户信息
            String openid = (String) map.get("openid");
            String nickname = (String) map.get("nickname");
            String headimgurl = (String) map.get("headimgurl");
            //保存用户信息到数据库中
            member = new Member();
            member.setNickname(nickname);
            member.setOpenid(openid);
            member.setAvatar(headimgurl);
            memberService.save(member);
        }

        //TODO 登录
        //生成jwt令牌
        String token = JwtUtils.getJwtToken(member.getId(), member.getNickname());
        //存入cookie
        //CookieUtils.setCookie(request,response,"jwt_token",token);
        //因为端口不同存在跨域问题 cookie并不能跨域，所以这里使用url重写
        return "redirect:http://localhost:3000?token="+token;
    }

    /**
     * 1 生成微信扫描二维码
     *
     * @param session
     * @return
     */
    @GetMapping("login")
    public String genWxCode(HttpSession session) {

        //微信开发平台授权baseUrl %s 相当于？代表占位符
        String baseUrl = "https://open.weixin.qq.com/connect/qrconnect" +
                "?appid=%s" +
                "&redirect_uri=%s" +
                "&response_type=code" +
                "&scope=snsapi_login" +
                "&state=%s" +
                "#wechat_redirect";

        //对redirectUrl进行URLEncoder编码
        String redirectUrl = ConstantPropertiesUtil.WX_OPEN_REDIRECT_URL;
        try {
            redirectUrl = URLEncoder.encode(redirectUrl, "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //设置 %s里面的值
        String url = String.format(
                baseUrl,
                ConstantPropertiesUtil.WX_OPEN_APP_ID,
                redirectUrl,
                "atguigu"
        );
        return "redirect:" + url;
    }
}
