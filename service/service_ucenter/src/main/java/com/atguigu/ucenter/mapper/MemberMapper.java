package com.atguigu.ucenter.mapper;

import com.atguigu.ucenter.entity.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author wangzhongxu
 * @since 2020-09-24
 */
public interface MemberMapper extends BaseMapper<Member> {

}
