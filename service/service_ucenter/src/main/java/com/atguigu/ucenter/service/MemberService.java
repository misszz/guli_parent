package com.atguigu.ucenter.service;

import com.atguigu.ucenter.entity.Member;
import com.atguigu.ucenter.entity.vo.LoginVo;
import com.atguigu.ucenter.entity.vo.RegisterVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author wangzhongxu
 * @since 2020-09-24
 */
public interface MemberService extends IService<Member> {
    public String login(LoginVo loginVo);

    public void register(RegisterVo registerVo);

    LoginVo getLoginInfo(String memberId);

    Member getByOpenid(String openId);
}
