package com.atguigu.ucenter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description 用户管理
 * @since 2020/9/24 16:04
 **/
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.atguigu.ucenter.mapper")
@ComponentScan(basePackages = {"com.atguigu"})
public class UcApplication {
    public static void main(String[] args) {
        SpringApplication.run(UcApplication.class, args);
    }
}
