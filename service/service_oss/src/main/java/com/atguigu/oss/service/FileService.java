package com.atguigu.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/7 21:51
 **/

public interface FileService {

    /**
     * 文件上传至阿里云
     * @param file
     * @return
     */
    String upload(MultipartFile file);
}
