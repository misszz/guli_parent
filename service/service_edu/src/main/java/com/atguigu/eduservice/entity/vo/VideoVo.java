package com.atguigu.eduservice.entity.vo;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/13 22:19
 **/

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
@ApiModel(value = "课时信息")
@Data
public class VideoVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String title;
    private Boolean free;
    private String videoOriginalName;
}