package com.atguigu.eduservice.entity.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/11 16:47
 **/
@Data
public class ExcelSubjectData {
    @ExcelProperty(index = 0)
    private String oneSubjectName;  //一级分类

    @ExcelProperty(index = 1)
    private String twoSubjectName;  //二级分类
}
