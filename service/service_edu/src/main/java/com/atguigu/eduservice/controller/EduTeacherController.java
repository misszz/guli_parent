package com.atguigu.eduservice.controller;


import com.atguigu.common.utils.R;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.query.TeacherQuery;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author wangzhongxu
 * @since 2020-08-31
 */
@RestController
@RequestMapping("/eduservice/teacher")
@Api(description = "讲师管理")
@CrossOrigin
public class EduTeacherController {
    @Autowired
    private EduTeacherService teacherService;
    @Autowired
    private EduCourseService courseService;

    @ApiOperation(value = "讲师列表")
    @GetMapping({"/list", "/"})
    public R list() {
        List<EduTeacher> list = teacherService.list(null);
        return R.ok().data("items", list);
    }

    @ApiOperation(value = "根据讲师Id删除讲师")
    @DeleteMapping("{id}")
    public R removeTeacher(@ApiParam(name = "id", value = "讲师Id", required = true) @PathVariable String id) {
        boolean result = teacherService.removeById(id);
        if (result){
            return R.ok();
        }else {
            return R.error().message("删除失败");
        }
    }

    @ApiOperation(value = "分页查询讲师列表")
    @GetMapping("pageTeacher/{page}/{limit}")
    public R pageList(@ApiParam(name = "page", value = "当前页码", required = true, defaultValue = "1")
                      @PathVariable("page") Long page,
                      @ApiParam(name = "limit", value = "每页记录数", required = true, defaultValue = "10")
                      @PathVariable("limit") Long limit) {

        Page<EduTeacher> pageParams = new Page<>(page, limit);
        Map<String, Object> map = teacherService.pageListWeb(pageParams);
        return R.ok().data(map);
//        teacherService.page(pageParams, null);
//        List<EduTeacher> teacherList = pageParams.getRecords();
//        long total = pageParams.getTotal();
//        return R.ok().data("total", total).data("rows", teacherList);
    }

    @ApiOperation(value = "分页条件查询讲师列表")
    @PostMapping("pageTeacherCondition/{page}/{limit}")
    public R pageQuery(@ApiParam(name = "page", value = "当前页码", required = true, defaultValue = "1")
                       @PathVariable("page") Long page,
                       @ApiParam(name = "limit", value = "每页记录数", required = true, defaultValue = "10")
                       @PathVariable("limit") Long limit,
                       @ApiParam(name = "teacherQuery", value = "查询对象", required = false)
                               @RequestBody(required = false) TeacherQuery teacherQuery) {
        //分页查询
        Page<EduTeacher> teacherPage = new Page<>(page, limit);

        teacherService.pageQuery(teacherPage, teacherQuery);
        List<EduTeacher> teacherList = teacherPage.getRecords();
        long total = teacherPage.getTotal();
        return R.ok().data("total", total).data("rows", teacherList);
    }

    @PostMapping("/save")
    @ApiOperation(value = "添加讲师")
    public R save(@ApiParam(name = "teacher",value = "讲师对象",required = true) @RequestBody EduTeacher teacher){
        teacherService.save(teacher);
        return R.ok();
    }

    @GetMapping("{id}")
    @ApiOperation(value = "通过id获取讲师")
    public R getTeacherOne(@ApiParam(name = "id",value = "讲师Id")
                                        @PathVariable("id") String id){
        EduTeacher teacher = teacherService.getById(id);
        //根据讲师id查询这个讲师的课程列表
        List<EduCourse> courseList = courseService.selectByTeacherId(id);
        System.out.println(">>>>>>>>>>>>>>>>>>>"+R.ok().data("item",teacher).data("courseList", courseList));
        return R.ok().data("item",teacher).data("courseList", courseList);
    }

    @PutMapping("/update")
    @ApiOperation(value = "通过id修改讲师")
    public R updateTeacherOne(@ApiParam(name = "teacher",value = "讲师对象",required = true) @RequestBody EduTeacher teacher){
        teacherService.updateById(teacher);
        return R.ok();
    }
}

