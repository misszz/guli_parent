package com.atguigu.eduservice.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.atguigu.config.exception.GuliException;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.vo.ExcelSubjectData;
import com.atguigu.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.util.Map;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/11 17:08
 **/

public class SubjectExcelListener extends AnalysisEventListener<ExcelSubjectData>{
    private EduSubjectService subjectService;
    public SubjectExcelListener(EduSubjectService subjectService){
        this.subjectService = subjectService;
    }
    //一行一行读取excel内容
    @Override
    public void invoke(ExcelSubjectData subjectData, AnalysisContext analysisContext) {
        if (subjectData == null){
            throw new GuliException(20002, "添加课程失败");
        }
        //添加分类
        //判断一级课程分类是否重复
        EduSubject oneSubject = this.isExistOneSubject(subjectData);
        if (oneSubject == null){
            oneSubject= new EduSubject();
            oneSubject.setTitle(subjectData.getOneSubjectName());
            oneSubject.setParentId("0");
            subjectService.save(oneSubject);
        }
        String id = oneSubject.getId();
        //添加二级分类 首先判断二级分类是否存在
        EduSubject twoSubject = this.isExistTwoSubject(subjectData, id);
        if (twoSubject == null){
            twoSubject = new EduSubject();
            twoSubject.setTitle(subjectData.getTwoSubjectName());
            twoSubject.setParentId(id);
            subjectService.save(twoSubject);
        }
        System.out.println(subjectData);
    }

    private EduSubject isExistTwoSubject(ExcelSubjectData subjectData,String id) {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("title", subjectData.getTwoSubjectName());
        wrapper.eq("parent_id", id);
        EduSubject eduSubject = subjectService.getOne(wrapper);
        return eduSubject;
    }

    private EduSubject isExistOneSubject(ExcelSubjectData subjectData) {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("title", subjectData.getOneSubjectName());
        wrapper.eq("parent_id", "0");
        EduSubject eduSubject = subjectService.getOne(wrapper);
        return eduSubject;
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头信息："+headMap);
    }
    //数据导入完成后调用
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        System.out.println("数据导入完成");
    }
}
