package com.atguigu.eduservice.service.impl;

import com.atguigu.config.exception.GuliException;
import com.atguigu.eduservice.entity.EduChapter;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.entity.vo.ChapterVo;
import com.atguigu.eduservice.entity.vo.VideoVo;
import com.atguigu.eduservice.mapper.EduChapterMapper;
import com.atguigu.eduservice.service.EduChapterService;
import com.atguigu.eduservice.service.EduVideoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author wangzhongxu
 * @since 2020-09-12
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {
    @Autowired
    private EduVideoService videoService;
    @Override
    public List<ChapterVo> nestedList(String courseId) {
        //最终要返回的数据列表
        List<ChapterVo> chapterVos = new ArrayList<>();

        //获取章节信息
        QueryWrapper<EduChapter> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId);
        wrapper.orderByAsc("sort","id");
        List<EduChapter> chapters = baseMapper.selectList(wrapper);

        //获取课时信息
        QueryWrapper<EduVideo> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("course_id", courseId);
        wrapper.orderByAsc("sort","id");
        List<EduVideo> videos = videoService.list(wrapper1);

        for (int i = 0; i < chapters.size(); i++) {
            //获取每个章节对应的所有课时
            EduChapter chapter = chapters.get(i);
            ChapterVo chapterVo = new ChapterVo();
            BeanUtils.copyProperties(chapter, chapterVo);
            List<VideoVo> videoList = new ArrayList<>();
            //遍历所有课时
            for (EduVideo video : videos) {
                if (video.getChapterId().equals(chapter.getId())){
                    VideoVo videoVo = new VideoVo();
                    BeanUtils.copyProperties(video, videoVo);
                    videoVo.setFree(video.getIsFree());
                    videoList.add(videoVo);
                }
            }
            chapterVo.setChildren(videoList);
            chapterVos.add(chapterVo);
        }
        return chapterVos;
    }

    @Override
    public boolean removeChapterById(String id) {
        //根据id查询是否存在视频 如果又则提示用户尚且又子节点
        if (videoService.getCountByChapterId(id)){
            throw new GuliException(20001, "该章节中存在视频课程,请删除视频课程");
        }
        Integer count = baseMapper.deleteById(id);
        //1.根据id删除章节
        return count != null && count > 0;
    }

    @Override
    public boolean removeByCourseId(String courseId) {
        QueryWrapper<EduChapter> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id", courseId);
        Integer count = baseMapper.delete(queryWrapper);
        return null != count && count > 0;
    }
}
