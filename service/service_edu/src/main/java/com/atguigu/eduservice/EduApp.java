package com.atguigu.eduservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/1 20:32
 **/
@SpringBootApplication
@ComponentScan(basePackages = {"com.atguigu"})
@EnableDiscoveryClient
@EnableFeignClients //启用feigin远程调用
public class EduApp {
    public static void main(String[] args) {
        SpringApplication.run(EduApp.class, args);
    }
}
