package com.atguigu.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atguigu.config.exception.GuliException;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.vo.ExcelSubjectData;
import com.atguigu.eduservice.entity.vo.SubjectNestVo;
import com.atguigu.eduservice.entity.vo.SubjectVO;
import com.atguigu.eduservice.listener.SubjectExcelListener;
import com.atguigu.eduservice.mapper.EduSubjectMapper;
import com.atguigu.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author wangzhongxu
 * @since 2020-09-11
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {
    @Autowired
    private EduSubjectMapper subjectMapper;
    @Override
    public void importBatchData(MultipartFile file,EduSubjectService subjectService) {
        try {
            InputStream in = file.getInputStream();
            EasyExcel.read(in, ExcelSubjectData.class, new SubjectExcelListener(subjectService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
            throw new GuliException(20002, "添加课程失败");
        }
    }

    @Override
    public List<SubjectNestVo> nestedList() {
        List<SubjectNestVo> subjectNestVos = new ArrayList<>();
        //1.查询一级分类
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id", "0");
        wrapper.orderByAsc("sort","id");
        List<EduSubject> eduSubjectList = subjectMapper.selectList(wrapper);

        for (EduSubject eduSubject : eduSubjectList) {
            SubjectNestVo nestVo = new SubjectNestVo();
            nestVo.setId(eduSubject.getId());
            nestVo.setTitle(eduSubject.getTitle());
            List<SubjectVO> vos = new ArrayList<>();
            //查询二级分类
            QueryWrapper<EduSubject> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("parent_id", eduSubject.getId());
            wrapper.orderByAsc("sort","id");
            List<EduSubject> eduSubjects = subjectMapper.selectList(queryWrapper);
            for (EduSubject subject : eduSubjects) {
                SubjectVO subjectVO = new SubjectVO();
                BeanUtils.copyProperties(subject, subjectVO);
                vos.add(subjectVO);
            }
            nestVo.setChildren(vos);
            subjectNestVos.add(nestVo);
        }
        return subjectNestVos;
    }
}
