package com.atguigu.eduservice.controller;


import com.atguigu.common.utils.R;
import com.atguigu.eduservice.entity.EduChapter;
import com.atguigu.eduservice.entity.vo.ChapterVo;
import com.atguigu.eduservice.service.EduChapterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author wangzhongxu
 * @since 2020-09-12
 */
@RestController
@RequestMapping("/eduservice/edu-chapter")
@CrossOrigin
@Api(description = "课程章节管理")
public class EduChapterController {

    @Autowired
    private EduChapterService chapterService;

    @ApiOperation(value = "嵌套章节数据列表")
    @GetMapping("nested-list/{courseId}")
    public R nestedListByCourseId(
            @ApiParam(name = "courseId", value = "课程ID", required = true)
            @PathVariable String courseId){

        List<ChapterVo> chapterVoList = chapterService.nestedList(courseId);
        return R.ok().data("items", chapterVoList);
    }

    @PostMapping
    @ApiOperation(value = "新增章节")
    public R save(@ApiParam(name = "chapterVo",value = "章节",required = true)
            @RequestBody EduChapter chapter){
        boolean row = chapterService.save(chapter);
        if (!row){
            return R.error().message("新增章节失败");
        }else {
            return R.ok().message("新增章节成功");
        }
    }

    @GetMapping("{id}")
    @ApiOperation(value = "根据id查询章节")
    public R getChapterOne(@ApiParam(name = "id",value = "章节id",required = true)
            @PathVariable String id){
        EduChapter chapter = chapterService.getById(id);
        return R.ok().data("items", chapter);
    }

    @ApiOperation(value = "根据ID修改章节")
    @PutMapping("{id}")
    public R updateById(
            @ApiParam(name = "id", value = "章节ID", required = true)
            @PathVariable String id,

            @ApiParam(name = "chapter", value = "章节对象", required = true)
            @RequestBody EduChapter chapter){

        chapter.setId(id);
        chapterService.updateById(chapter);
        return R.ok();
    }

    @ApiOperation(value = "根据ID删除章节")
    @DeleteMapping("{id}")
    public R removeById(
            @ApiParam(name = "id", value = "章节ID", required = true)
            @PathVariable String id){

        boolean result = chapterService.removeChapterById(id);
        if(result){
            return R.ok();
        }else{
            return R.error().message("删除失败");
        }
    }
}

