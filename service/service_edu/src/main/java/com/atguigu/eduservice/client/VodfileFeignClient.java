package com.atguigu.eduservice.client;

import com.atguigu.common.utils.R;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/21 21:32
 **/
@Component
public class VodfileFeignClient implements VodClient{

    @Override
    public R removeVideo(String videoId) {
        return R.error().message("time out");
    }

    @Override
    public R removeVideoList(List<String> videoIdList) {
        return R.error().message("time out");
    }

    @Override
    public R index() {
        return R.error().message("首页信息展示出错");
    }
}
