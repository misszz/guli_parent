package com.atguigu.eduservice.entity.vo;

import lombok.Data;

import java.util.List;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/12 0:33
 **/
@Data
public class SubjectNestVo {
    private String id;
    private String title;
    private List<SubjectVO> children;
}
