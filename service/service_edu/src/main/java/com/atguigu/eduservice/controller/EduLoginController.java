package com.atguigu.eduservice.controller;

import com.atguigu.common.utils.R;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/5 13:50
 **/
@RestController
@RequestMapping("/eduservice/user")
@CrossOrigin    //解决跨域
public class EduLoginController {

    @PostMapping("login")
    public R login(){
        return R.ok().data("token","admin");
    }

    @GetMapping("info")
    public R info(){
        return R.ok().data("roles","[admin]").data("name","admin");
    }
}
