package com.atguigu.eduservice.client;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/20 21:04
 **/
@FeignClient(value = "service-vod",fallback = VodfileFeignClient.class)
@Component
public interface VodClient {
    /**
     * 定义调用的方法路径
     * 根据视频id删除阿里云视频
     * @pathvariable 注解一定要指定参数名称,否则报错
     * @param videoId
     * @return
     */
    @DeleteMapping("/vod/video/{videoId}")
    public R removeVideo(@PathVariable("videoId") String videoId);

    @DeleteMapping(value = "/vod/video/delete-batch")
    public R removeVideoList(@RequestParam("videoIdList") List<String> videoIdList);

    @GetMapping("/eduservice/index/index")
    public R index();
}
