package com.atguigu.eduservice.entity.vo;

import lombok.Data;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/11 23:49
 **/
@Data
public class SubjectVO {

    private String id;
    private String title;
}
