package com.atguigu.eduservice.entity.vo;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/13 22:18
 **/

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "章节信息")
@Data
public class ChapterVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String title;
    private List<VideoVo> children = new ArrayList<>();
}
