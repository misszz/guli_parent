package com.atguigu.vod.service;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/20 14:47
 **/

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface VideoService {
    String uploadVideo(MultipartFile file);

    void removeVideo(String videoId);

    void removeVideoList(List<String> videoIdList);
}