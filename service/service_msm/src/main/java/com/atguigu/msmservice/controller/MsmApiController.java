package com.atguigu.msmservice.controller;

import com.atguigu.common.utils.R;
import com.atguigu.msmservice.service.MsmService;
import com.atguigu.msmservice.utils.RandomUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/24 14:59
 **/

@RestController
@RequestMapping("/edumsm")
@CrossOrigin //跨域
@Api(description = "短信服务")
public class MsmApiController {

    @Autowired
    private MsmService msmService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @GetMapping(value = "/send/{phone}")
    @ApiOperation(value = "短信发送")
    public R code(@PathVariable String phone) {
        String code = redisTemplate.opsForValue().get(phone);
        if(!StringUtils.isEmpty(code)) return R.ok();
        code = RandomUtil.getSixBitRandom();
        Map<String,Object> param = new HashMap<>();
        param.put("code", code);
        boolean isSend = msmService.send(phone, "SMS_169111508", param);
        if(isSend) {
            redisTemplate.opsForValue().set(phone, code,5, TimeUnit.HOURS);
            return R.ok();
        } else {
            return R.error().message("发送短信失败");
        }
    }
}