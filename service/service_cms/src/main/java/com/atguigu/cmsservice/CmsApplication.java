package com.atguigu.cmsservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/23 9:38
 **/
@SpringBootApplication
@MapperScan("com.atguigu.cmsservice.mapper")
@ComponentScan(basePackages = {"com.atguigu"})
@EnableDiscoveryClient
public class CmsApplication {
    public static void main(String[] args) {
        SpringApplication.run(CmsApplication.class, args);
    }
}
