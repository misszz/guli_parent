package com.atguigu.cmsservice.controller;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/9/23 10:41
 **/

@RestController
public class TestController {
    @GetMapping(value = "/test")
    @Cacheable(value = "name",key = "#name")
    public String test(String name){
        System.out.println(".............");
        return "123";
    }
}
